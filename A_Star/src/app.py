import pygame
import math

class Node():
  def __init__(self,x,y):
    self.pos_x  = x      #Coordenadas para el nodo
    self.pos_y  = y
    self.id     = 0
class Board():
  # permitido = 0, bloqueado = 1 ,nodo_ini = 2, nodo_fin = 3, anteriores = 4 
  matriz = []
  def __init__(self,col,fil):
    self.num_col  = col
    self.num_fil  = fil 
    #Lleno la matriz de nodos
    for i in range(fil):
      self.matriz.append([])
      for j in range(col):
        self.matriz[i].append( Node(i,j) )   #Instancio mis nodos :
    ##################################
  def set_node_ini(self,x,y):  
    self.matriz[x][y].id = 2
    #print self.matriz[x][y].id
  def set_node_fin(self,x,y):
    self.matriz[x][y].id = 3



class A_Star():
  tablero     = 0
  nodo_actual = () #una tupla para x,y
  nodo_fin    = () #una tupla para x,y
  list_open   = []  #Nodos por visitar
  list_close  = []  #Nodos ya visitados
  def __init__(self, dimension_x,dimension_y):
    self.tablero = Board(dimension_x,dimension_y)

  def distance(self, x1,y1,x2,y2):
    return math.sqrt( ((y2-y1)**2) + ((x2-x1)**2) )
  
  def is_valid(self,x,y):  #Le pasare un punto(x,y) para que verifique si pertenece a mi tablero
    fil = self.tablero.num_fil
    col = self.tablero.num_col
    #Son accesibles todos menos los negativos y los de ID 1 qye son los obstaculos
    if ( (x>=0 and x<fil) and (y>=0 and y<col) ) and (self.tablero.matriz[x][y].id == 0 or self.tablero.matriz[x][y].id == 3 or self.tablero.matriz[x][y].id == 2):  
      return True 
    else:
      return False
  def comparar(self,x,y):  #Esta funcion me devuelve los vecinos de un punto e inserta el nodo actual en la lista cerrada 
    i = -1  #Iteradores 
    j = -1
    while i<2 :
      j = -1
      while j<2:
        xp =  i + x
        xn =  j + y
        if (self.is_valid(xp,xn)) :
          tupla = (xp,xn)
          self.list_open.append(tupla)  #A mi lista le agrego una tupla
        j = j+1 
      i=i+1
    #Remuevo el mismo que no deberia compararse 
    tupla_entra = (x,y)
    #QUITO DE MI LISTA ABIERTA  EL PUNTO EN EL QUE ME ENCUENTRO PORQUE NO VOY A COMPARAR CONMIGO MISMO
    self.list_open.remove(tupla_entra)   
     
  def get_path(self,x_actual,y_actual,x_fin,y_fin,recorrido,camino_optimo):
    mejor_nodo   = (0,0)
    self.comparar(x_actual,y_actual)       #Consigo mi lista de vecinos para x_ini,y_ini
    i     = len(self.list_open)
    mejor = 1000000
    ite   = 0
    while ite < i:
      tuple_ = self.list_open[ite]       # Saco uno a uno las tuplas de la lista para comparar
      h   = self.distance(tuple_[0],tuple_[1], self.nodo_fin[0],self.nodo_fin[1] )  #al nodo final
      g   = self.distance(self.nodo_actual[0],self.nodo_actual[1], tuple_[0],tuple_[1])   #al vecino
      funcion_f = g + h
      if funcion_f < mejor:
        mejor      = funcion_f
        mejor_nodo = (tuple_[0],tuple_[1]) 
      ite = ite+1 
    self.list_close.append(mejor_nodo)  #Consigo el mejor nodo
    self.nodo_actual = (mejor_nodo[0],mejor_nodo[1])       #Actualizo mi nodo actual
    print "mejor nodo"
    print self.nodo_actual
    for x in self.list_open[:]:
      self.list_open.remove(x)
        #self.list_open.remove(x)
    #Cada vez que llego a un nuevo nodo mejor, el anterior quiere decir que ya fue visitado.
    self.tablero.matriz[ x_actual ][ y_actual ].id = 4
    

  def empezar(self,dimension_x,dimension_y,x_ini,y_ini ,x_fin ,y_fin):         #Funcion que encapsula todo 
    recorrido     = []     #Todo lo que recorri para llegar al mejor nodo
    camino_optimo = []     #El camino final optim
    node  = Board(dimension_x,dimension_y)  #Inicializo el tablero
    #node.set_node_ini(x_ini,y_ini) #Indico el nodo inicial
    #node.set_node_fin(x_fin,y_fin) #Indico el nodo final
    #OBTENIENDO TODO EL CAMINO
    self.nodo_actual  = (x_ini,y_ini) 
    self.nodo_fin     = (x_fin,y_fin)
    self.list_close.append ( (x_ini,y_ini) )  #Siempre agrego el primer elemento primero
    while self.nodo_actual != self.nodo_fin:
      self.get_path( self.nodo_actual[0],self.nodo_actual[1],self.nodo_fin[0],self.nodo_fin[1],recorrido,camino_optimo)  #Obtengo el camino 

def main():
     dimension_x   = 1000
     dimension_y   = 1000
     x_ini = 0
     y_ini = 0
     x_fin = 0
     y_fin = 0

     A_star = A_Star(dimension_x,dimension_y)
    #OBjeto nodo
     OBJECT_NODO = Board(dimension_x,dimension_y)
    # Definimos algunos colores
     NEGRO = (0,0,0)
     BLANCO = (255,255,255)
     VERDE  = (0,255,0)
     ROJO   = (255,0,0)
 
# Establecemos el LARGO y ALTO de cada celda de la retcula.
     LARGO  = 20
     ALTO   = 20
 
# Establecemos el margen entre las celdas.
     MARGEN = 5

     running   = True;
     pygame.init()                                              #Inicializo el modulo para utilizar la libreria

    #Dimension de la pantalla
     ancho =  dimension_x * 25
     largo =  dimension_y * 25
     screen= pygame.display.set_mode((ancho,largo))         #Dimensiones de la ventana
     pygame.display.set_caption("A STAR")                  #Titulo de la ventana 
     reloj1       = pygame.time.Clock()
     control_loop = 0


     eso = 0
     while running != False:
          for event in pygame.event.get():
               if event.type == pygame.QUIT:
                    running = False;
               elif event.type == pygame.MOUSEBUTTONDOWN:
                # El usuario presiona el raton. Obtiene su posicion.
                    pos = pygame.mouse.get_pos()    #return a tuple
                
                # Cambia las coordenadas x/y de la pantalla por coordenadas reticulares
                    columna = pos[0] / (LARGO + MARGEN)  #Convirtiendo coordenadas 
                    fila    = pos[1] / (ALTO + MARGEN)   #Convirtiendo
                # Establece punto de inicio y fin 
                    if(control_loop == 0 ):
                         OBJECT_NODO.set_node_ini(fila,columna) #Indico el nodo inicial
                         x_ini = fila 
                         y_ini = columna

                    if  control_loop == 1 :
                         OBJECT_NODO.set_node_fin(fila,columna) #Indico el nodo final
                         x_fin = fila
                         y_fin = columna
                    if(control_loop != 0 and control_loop != 1): #despues de colocar los puntos verdes coloco los obstaculos
                         OBJECT_NODO.matriz[fila][columna].id = 1 #Indico el nodo inicial
                         
                    control_loop = control_loop +1
                    print("Click ", pos, "Coordenadas de la reticula: ", fila, columna)
               elif event.type == pygame.KEYDOWN and event.key == pygame.K_ESCAPE:   #Cuando detecto el ESCAPE calculo
                    print "HUBO UN ENTER"
                    eso = 1
                    A_star.empezar(dimension_x,dimension_y,x_ini,y_ini ,x_fin ,y_fin)
                    print "elementos"
                    size = len(A_star.list_close) 
                    print size
                    i = 0
                              #pygame.draw.line(screen,VERDE,(0,0),(75,75))
                              #AGREGO EL PUNTO FINAL A LA LISTA SIEMPRE
                    while i < (size):
                         tuple2 = A_star.list_close[i]
                         print tuple2
                         i = i+1
                    

                
        # Establecemos el fondo de pantalla.
          screen.fill(NEGRO)
        # Dibujamos la reticula
          for fila in range(dimension_x):
            #print fil
               for columna in range(dimension_y):
                    color = BLANCO
                    if OBJECT_NODO.matriz[fila][columna].id  == 2  :
                         color = VERDE
                    if OBJECT_NODO.matriz[fila][columna].id  == 3 :         
                         color = VERDE
                         if eso == 1:
                              #CALCULANDO 
                              size = len(A_star.list_close) 
                              i = 0
                              doce = 15
                              #pygame.draw.line(screen,VERDE,(0,0),(75,75))
                              #AGREGO EL PUNTO FINAL A LA LISTA SIEMPRE
                              while i < (size-1):
                                   tuple1 = A_star.list_close[i]
                                   tuple2 = A_star.list_close[i+1]
                                   #pygame.draw.line(screen,VERDE,(15,15),(63,86))
                                   pygame.draw.line(screen,ROJO,( (MARGEN+LARGO) * tuple1[1]+ doce,
                                                          (MARGEN+LARGO) *tuple1[0]+doce),
                                                          ((MARGEN+LARGO) *tuple2[1]+doce,
                                                          (MARGEN+LARGO) *tuple2[0]+doce),5)
                                   i=i+1
                         ##$##############"""
                    if OBJECT_NODO.matriz[fila][columna].id  == 1  :
                         color = ROJO

                    pygame.draw.rect(screen,
                                 color,
                                 [(MARGEN+LARGO) * columna + MARGEN,
                                  (MARGEN+ALTO) * fila + MARGEN,
                                  LARGO,
                                  ALTO])
         
        # Limitamos a 20 fotogramas por segundo.
          reloj1.tick(20)
        # Avanzamos y actualizamos la pantalla con lo que hemos dibujado.
          pygame.display.flip()
pygame.quit()


main()